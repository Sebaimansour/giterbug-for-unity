﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.Video;

namespace InspectorVideoPack 
{
    public class InspectorVideoWindow : EditorWindow
    {
        private string packPath = "Assets/InspectorVideoPack/";
        private GameObject inspectorVideoGObj = null;
        private VideoPlayer videoPlayer = null;

        [MenuItem("Window/Inspector Video")]
        public static void ShowWindow()
        {
            // Get existing open window or if none, make a new one:
            EditorWindow.GetWindow(typeof(InspectorVideoWindow));
        }

        private void Awake()
        {
            titleContent.text = " Inspector Video";
            titleContent.image = EditorGUIUtility.Load(packPath+"Textures/TitleIcon.png") as Texture2D;

            if (!inspectorVideoGObj)
            {
                InstantiateGameObjects();
            }
        }

        private void InstantiateGameObjects()
        {
            inspectorVideoGObj = Instantiate(EditorGUIUtility.Load(packPath+"Prefabs/InspectorVideo.prefab")) as GameObject;
            videoPlayer = inspectorVideoGObj.GetComponent<InspectorVideo>().videoPlayer;
        }

        private void OnDestroy()
        {
            DestroyImmediate(inspectorVideoGObj);
        }

        private void OnGUI()
        {
            if (!videoPlayer)
            {
                Close();
            }

            Texture texture = videoPlayer.targetTexture;
            float boxWidth = EditorGUIUtility.currentViewWidth - 6.0f;
            float boxHeihgt = boxWidth * texture.height / texture.width;

            GUILayout.Label(texture, GUILayout.Width(boxWidth), GUILayout.Height(boxHeihgt));

            GUILayout.BeginHorizontal();

            if (videoPlayer.isPlaying)
            {
                if (GUILayout.Button(EditorGUIUtility.Load(packPath+"Textures/pause-icon.png") as Texture2D, GUILayout.Width(22), GUILayout.Height(16)))
                {
                    videoPlayer.Pause();
                }
            }
            else
            {
                if (GUILayout.Button(EditorGUIUtility.Load(packPath+"Textures/play-icon.png") as Texture2D, GUILayout.Width(22), GUILayout.Height(16)))
                {
                    videoPlayer.Play();
                }
            }

            float playTime = (float)videoPlayer.time;
            int playTimeS = (int)playTime;
            int playTimeM = playTimeS / 60;
            playTimeS = playTimeS % 60;
            int playTimeH = playTimeM / 60;
            playTimeM = playTimeM % 60;

            GUILayout.Label(playTimeH.ToString() + ":" + playTimeM.ToString("D2") + ":" + playTimeS.ToString("D2"), GUILayout.Width(45));

            float custPlayTime = GUILayout.HorizontalSlider(playTime, 0.0f, (float)videoPlayer.clip.length);

            if (custPlayTime != playTime)
            {
                videoPlayer.time = custPlayTime;

                if (!videoPlayer.isPlaying)
                {
                    videoPlayer.Play();
                    videoPlayer.Pause();
                }
            }

            GUILayout.Space(10.0f);

            GUILayout.Label(EditorGUIUtility.Load(packPath+"Textures/sound-icon.png") as Texture2D, GUILayout.Width(20), GUILayout.Height(18));

            float playVolume = videoPlayer.GetDirectAudioVolume(0);
            float custPlayVolume = GUILayout.HorizontalSlider(playVolume, 0.0f, 1.0f, GUILayout.Width(50));

            if (custPlayVolume != playVolume)
            {
                videoPlayer.SetDirectAudioVolume(0, custPlayVolume);
            }

            GUILayout.EndHorizontal();

            DrawUILine(Color.gray);

            GUIStyle paraStyle = new GUIStyle();
            paraStyle.wordWrap = true;
            paraStyle.padding = new RectOffset(10, 10, 10, 10);
            GUILayout.Label(
                "<b>Issue #36</b>\n" +
                "\n" +
            	"Steps to recreate -\n" +
            	"\n" +
            	"1. Go home screen\n" +
            	"2. Click on a fish\n" +
            	"3. Click option\n" +
            	"4. Then select Red\n" +
            	"\n" +
                "<b>Notes:</b> If you already played the game, you have to restart the game to recreate it. If the cellular data is off, it also does not show error.", 
                paraStyle);
        }

        private void Update()
        {
            Repaint();
        }

        private void DrawUILine(Color color, int thickness = 2, int padding = 10)
        {
            Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            r.height = thickness;
            r.y += padding / 2;
            r.x -= 2;
            r.width += 6;
            EditorGUI.DrawRect(r, color);
        }

    }
}

