﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace InspectorVideoPack
{
    [CustomEditor(typeof(InspectorVideo))]
    public class InspectorVideoEditor : Editor
    {
        public override void OnInspectorGUI()
        {
        }
    }
}
