﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

namespace InspectorVideoPack
{
    [ExecuteInEditMode]
    public class InspectorVideo : MonoBehaviour
    {
        public VideoPlayer videoPlayer;

        private void Awake()
        {
            videoPlayer = transform.Find("VideoPlayer").GetComponent<VideoPlayer>();
        }
    }
}
