﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugReportUI : MonoBehaviour
{
    public GameObject[] panels;
    private int curPage = 0;

    public void OnBack()
    {
        if (curPage > 0)
            ShowPanel(curPage - 1);
    }

    public void OnNext()
    {
        ShowPanel(curPage + 1);
    }

    public void OnFinish()
    {
        ShowPanel(0);
    }

    public void ShowPanel(int page)
    {
        curPage = page;
        for(int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(i == page);
        }
    }
}
